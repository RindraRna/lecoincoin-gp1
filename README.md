# lecoincoin-gp1

**Informations générales**   
Lecoincoin est une application de gestion de dépôt vente touchant à tout ce qui peut se vendre ou s’acheter.  
C'est donc une application de backend permettant l'administration des annonces: modification, suppression et création.  
Mr Gerard Lecoincoin est l'administrateur de l'application et Mme Mathilde Lecoincoin est la modératrice.

**Démarrage**    
Pour démarrer le projet, faites les étapes suivantes:  
1. Ouvrir le projet avec IntelliJ  
2. Aller dans grails-app/conf/application.yml  
3. Aller à la ligne 153 et changer la valeur de path par le chemin du dossier pointant vers "images" du projet.  
N'oublier pas le "/" à la fin.  
ex: G:/programmations/ITU/MBDS/Gregory Galli/Projets/Lecoincoi/lecoincoin/grails-app/assets/images/  
4. Lancer l'application avec le boutton Run de IntelliJ    

**Auteur**    
L'application a été conçue par le groupe 1 composé de:  
. ANDRIANARIVO Henintsoa Jonatana           n°07  
. MIHARINIAINA Andriamihanta Rilah Mario    n°12  
. RABESANDRATANA Laza Nomentsoa             n°14  
. RAMIANDRISOA Rindra Ny Aina               n°34

**Fonctionnalités**    
Les fonctionnalités développées sont:  
1. Login avec spring security: si les données entrées sont fausses, on revient sur la page de login avec un message d'erreur   
2. Création de rôles des utilisateurs:  
- ROLE_ADMIN: Il peut tout faire: créer, modifier, supprimer, lire  
Login  admin: Gérard/password  
- ROLE_MODERATTEUR:  Il ne peut que lire et modifier  
Login modérateur: Mathilde/password  
3. Gestion des utilisateurs:  
- Ajout d'un nouvel utilisateur  
- Suppression d'un utilisateur  
- Modification d'un utilisateur: nom, mot de passe et rôle  
- Lecture des utilisateurs  
4. Gestion des annonces:  
- Ajout d'une nouvelle annonce  
- Suppression d'une annonce  
- Modification d'une annonce  
- Lecture des annonces  
5. Gestion des illustrations via l'interface des annonces:  
- Ajout d'une ou plusieurs illustrations  
- Suppression d'une illustration  
- Lecture des illustrations d'une annonce        

**Fonctionnalités en plus**    
En dehors des fonctionnalités demandées, le groupe 1 a ajouté d'autres fonctionnalités:
1. Le tableau de bord pour l'application:
- Affichage du nombre de données dans chaque table 
- Affichage du pourcentage de données dans chaque table dans un progressbar
- Affichage de la répatition des rôles des utilisateurs dans un pie chart
2. Changer le thème de l'application: couleur noir ou bleu
3. La recherche des annonces sur le titre ou la description de l'application    
4. La gestion de mot de passe:
- Si on ajoute un utilisateur, il y aura un champ de confirmation de mot de passe qui vérifiera si les 2 champs de mot de passe sont bien identiques.
- Si on modifie un utilisateur, seul l'utilisateur connecté peut modifier son mot de passe.  
ex: Si Gérard est connecté, il ne peut pas modifier le mot de passe de Mathilde. Il ne peut que modifier le mot de passe de son compte.  
5. Pagination lors de l'affichage des annonces (10 par page), la recherche des annonces (10 par page) et les utilisateurs (5 par pages)

**API**    
Les API développés peuvent être testés via la collection POSTMAN dans le code source: 12 api.postman_collection.json  
Pour tester la collection, veuillez lancer le projet et créer une variable tokenLogin avec l'access_login du login comme valeur.  
Ces API sont:
1. POST http://localhost:8080/Lecoincoin/api/login : s'identifier en tant que Gérard
2. GET http://localhost:8080/Lecoincoin/api/annonces : récupérer toutes les annonces
3. POST http://localhost:8080/Lecoincoin/api/annonces: ajout d'une nouvelle annonce
4. GET http://localhost:8080/Lecoincoin/api/annonce/1 : récupérer l'annonce avec l'id = 1
5. PUT http://localhost:8080/Lecoincoin/api/annonce/1 : modifier l'annonce avec l'id = 1 avec PUT
6. PATCH http://localhost:8080/Lecoincoin/api/annonce/1: modifier l'annonce avec l'id = 1 avec PATCH
7. DELETE http://localhost:8080/Lecoincoin/api/annonce/1 : supprimer l'annonce avec l'id 1
8. GET http://localhost:8080/Lecoincoin/api/users : récupérer toutes les utilisateur
9. POST http://localhost:8080/Lecoincoin/api/users: ajout d'un nouvel utilisateur
10. GET http://localhost:8080/Lecoincoin/api/user/2 : récupérer l'utilisateur avec l'id = 2
11. PUT http://localhost:8080/Lecoincoin/api/user/2 : modifier l'utilisateur avec l'id = 2 avec PUT
12. PATCH http://localhost:8080/Lecoincoin/api/user/2: modifier l'utilisateur avec l'id = 2 avec PATCH
13. DELETE http://localhost:8080/Lecoincoin/api/user/2 : supprimer l'utilisateur avec l'id 2