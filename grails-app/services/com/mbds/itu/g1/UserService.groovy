package com.mbds.itu.g1

import grails.gorm.services.Service

@Service(User)
class UserService {
    def modifier(User ancienUser, User userModifie){
        ancienUser.username= userModifie.username
        ancienUser.password = userModifie.password
        ancienUser.save()
    }

    UserRole findUserRole(User u){
        UserRole.where { user == u }.get()
    }

    User get(Serializable id){
        User.get(id)
    }

    List<User> list(){
        User.list()
    }

    Long count(){
        User.count()
    }

    Long countRole(){
        Role.count()
    }

    void delete(Serializable id){
        User.get(id).delete()
    }

    User save(User user){
        user.save()
    }

    List<Role> listRole(){
        Role.list()
    }

    int findByRole(Role role){
        UserRole.findAllByRole(role).size()
    }

    User enleverLesNull(User user1, User user2){
        if(user2.username == null) user2.username = user1.username
        if(user2.password == null) user2.password = user1.password
        return user2
    }
}
