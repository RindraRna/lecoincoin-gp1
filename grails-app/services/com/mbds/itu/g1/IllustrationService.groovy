package com.mbds.itu.g1

import grails.gorm.services.Service

@Service(Illustration)
interface IllustrationService {
    void delete(Serializable id)
    Long count()

}
