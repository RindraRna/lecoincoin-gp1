package com.mbds.itu.g1

import grails.converters.JSON
import grails.converters.XML
import grails.gorm.transactions.Transactional

import javax.servlet.http.HttpServletResponse

@Transactional
class ApiService {
    Api api = new Api()
    def annonceService
    def userService

//    GET / PUT / PATCH / DELETE
//    url : localhost:8080/Lecoincoin/api/user/{id}
    def user(def methode, def id, def format, def userModifie, def role) {
        if (!id) {
            api.setStatus(400)
            api.setMessage("Erreur BAD REQUEST: La requête est mal formatée")
            api.setData("Aucun")
            return serializeData(api, format)
        }
        else{
            def userInstance = userService.get(id)
            if (!userInstance){
                api.setStatus(404)
                api.setMessage("Erreur NOT FOUND: L'annonce n'est pas dans la base")
                api.setData("Aucun")
                return serializeData(api, format)
            }
            else{
                switch (methode) {
                    case "GET":
                        api.setStatus(200)
                        api.setMessage("OK: L'utilisateur a été récupéré")
                        api.setData(userInstance)
                        return serializeData(api, format)
                        break
                    case "PUT":
                        if(userModifie.username == null || userModifie.password == null || role == null){
                            api.setStatus(400)
                            api.setMessage("Erreur BAD REQUEST: Vous n'avez pas envoyé toutes les données (username, password, role)")
                            api.setData("Aucun")
                            return serializeData(api, format)
                        }
                        else{
                            if(role == "ROLE_MODERATEUR" || role == "ROLE_ADMIN"){
                                /*modifier user*/
                                userService.modifier(userInstance, userModifie)

                                /*modifiere role user*/
                                def roleAuthority = Role.findByAuthority(role)
                                UserRole.removeAll(userInstance)
                                def role2 = Role.get(roleAuthority.id)
                                User nouveauUser = userService.get(userInstance.id)
                                UserRole.create(nouveauUser, role2,true)

                                api.setStatus(200)
                                api.setMessage("OK: L'utilisateur a été modifié")
                                api.setData("Aucun")
                                return serializeData(api, format)
                            }
                            else{
                                api.setStatus(400)
                                api.setMessage("Erreur BAD REQUEST: La valeur de role doit être \"ROLE_MODERATEUR\" ou bien \"ROLE_ADMIN\"")
                                api.setData("Aucun")
                                return serializeData(api, format)
                            }
                        }
                        break
                    case "PATCH":
                        User userSansNull = userService.enleverLesNull(userInstance, userModifie)
                        /*modifier user*/
                        userService.modifier(userInstance, userSansNull)

                        /*modifiere role user*/
                        if(role == "ROLE_MODERATEUR" || role == "ROLE_ADMIN"){
                            def roleAuthority = Role.findByAuthority(role)
                            UserRole.removeAll(userInstance)
                            def role2 = Role.get(roleAuthority.id)
                            User nouveauUser = userService.get(userInstance.id)
                            UserRole.create(nouveauUser, role2,true)
                        }
                        else{
                            api.setStatus(400)
                            api.setMessage("Erreur BAD REQUEST: La valeur de role doit être \"ROLE_MODERATEUR\" ou bien \"ROLE_ADMIN\"")
                            api.setData("Aucun")
                            return serializeData(api, format)
                        }

                        api.setStatus(200)
                        api.setMessage("OK: L'utilisateur a été modifié")
                        api.setData("Aucun")
                        return serializeData(api, format)
                        break
                    case "DELETE":
                        /*suppression user dans userRole*/
                        def user=userService.get(id)
                        def userRole = userService.findUserRole(user)
                        if(userRole) UserRole.remove(user, userRole.role)

                        /*suppression user*/
                        userService.delete(id)

                        api.setStatus(200)
                        api.setMessage("OK: L'utilisateur a été supprimé")
                        api.setData("Aucun")
                        return serializeData(api, format)
                        break
                    default:
                        api.setStatus(405)
                        api.setMessage("Erreur METHOD NOT ALLOWED: La méthode de la requête n'est pas acceptée")
                        api.setData("Aucun")
                        return serializeData(api, format)
                        break
                }
            }
        }
        api.setStatus(406)
        api.setMessage("Erreur NOT ACCEPTABLE: Impossible de servir une réponse")
        api.setData("Aucun")
        return serializeData(api, format)
    }

//    GET / POST
//    url : localhost:8080/Lecoincoin/api/users
    def users(def methode, def format, def nouveauUser, def role) {
        switch (methode) {
            case "GET":
                def users = userService.list()
                api.setStatus(200)
                api.setMessage("OK: Tous les utilisateurs ont été récupérés")
                api.setData(users)
                return serializeData(api, format)
                break
            case "POST":
                if(nouveauUser.username == null || nouveauUser.password == null || role == null){
                    api.setStatus(400)
                    api.setMessage("Erreur BAD REQUEST: Vous n'avez pas envoyé toutes les données (username, password, role)")
                    api.setData("Aucun")
                    return serializeData(api, format)
                }
                else{
                    if(role == "ROLE_MODERATEUR" || role == "ROLE_ADMIN"){
                        /*ajout user*/
                        userService.save(nouveauUser)

                        /*ajout role modérateur de user*/
                        def authorityValeur = Role.findByAuthority(role)
                        UserRole.create(nouveauUser, authorityValeur , true)

                        api.setStatus(200)
                        api.setMessage("OK: L'utilisateur a été ajouté")
                        api.setData("Aucun")
                        return serializeData(api, format)
                    }
                    else{
                        api.setStatus(400)
                        api.setMessage("Erreur BAD REQUEST: La valeur de role doit être \"ROLE_MODERATEUR\" ou bien \"ROLE_ADMIN\"")
                        api.setData("Aucun")
                        return serializeData(api, format)
                    }
                }
                break
            default:
                api.setStatus(405)
                api.setMessage("Erreur METHOD NOT ALLOWED: La méthode de la requête n'est pas acceptée")
                api.setData("Aucun")
                return serializeData(api, format)
                break
        }
        api.setStatus(406)
        api.setMessage("Erreur NOT ACCEPTABLE: Impossible de servir une réponse")
        api.setData("Aucun")
        return serializeData(api, format)
    }

//    GET / POST
//    url : localhost:8080/Lecoincoin/api/annonces
    def annonces(def methode, def format, def nouvelleAnnonce){
        switch (methode) {
            case "GET":
                def annonces = annonceService.getAnnonces()
                api.setStatus(200)
                api.setMessage("OK: Toutes les annonces ont été récupérées")
                api.setData(annonces)
                return serializeData(api, format)
                break
            case "POST":
                if(nouvelleAnnonce.title == null || nouvelleAnnonce.description == null || nouvelleAnnonce.price == null || nouvelleAnnonce.author == null){
                    api.setStatus(400)
                    api.setMessage("Erreur BAD REQUEST: Vous n'avez pas envoyé toutes les données (title, description, price, author{id})")
                    api.setData("Aucun")
                    return serializeData(api, format)
                }
                else{
                    annonceService.save(nouvelleAnnonce)
                    api.setStatus(200)
                    api.setMessage("OK: L'annonce a été ajoutée")
                    api.setData("Aucun")
                    return serializeData(api, format)
                }
                break
            default:
                api.setStatus(405)
                api.setMessage("Erreur METHOD NOT ALLOWED: La méthode de la requête n'est pas acceptée")
                api.setData("Aucun")
                return serializeData(api, format)
                break
        }
        api.setStatus(406)
        api.setMessage("Erreur NOT ACCEPTABLE: Impossible de servir une réponse")
        api.setData("Aucun")
        return serializeData(api, format)
    }

    //    GET / PUT / PATCH / DELETE
    //   url : localhost:8080/Lecoincoin/api/annonce/{id}
    def annonce(def methode, def id, def format, def annonceModifie){
        if (!id) {
            api.setStatus(400)
            api.setMessage("Erreur BAD REQUEST: La requête est mal formatée")
            api.setData("Aucun")
            return serializeData(api, format)
        }
        else{
            def annonceInstance = annonceService.getById(id)
            if (!annonceInstance){
                api.setStatus(404)
                api.setMessage("Erreur NOT FOUND: L'annonce n'est pas dans la base")
                api.setData("Aucun")
                return serializeData(api, format)
            }
            else{
                switch (methode) {
                    case "GET":
                        api.setStatus(200)
                        api.setMessage("OK: L'annonce a été récupérée")
                        api.setData(annonceInstance)
                        return serializeData(api, format)
                        break
                    case "PUT":
                        if(annonceModifie.title == null || annonceModifie.description == null || annonceModifie.price == null){
                            api.setStatus(400)
                            api.setMessage("Erreur BAD REQUEST: Vous n'avez pas envoyé toutes les données (title, description, price)")
                            api.setData("Aucun")
                            return serializeData(api, format)
                        }
                        else{
                            annonceService.modifier(annonceInstance, annonceModifie)
                            api.setStatus(200)
                            api.setMessage("OK: L'annonce a été modifiée")
                            api.setData("Aucun")
                            return serializeData(api, format)
                        }
                        break
                    case "PATCH":
                        Annonce annonceSansNull = annonceService.enleverLesNull(annonceInstance, annonceModifie)
                        annonceService.modifier(annonceInstance, annonceSansNull)
                        api.setStatus(200)
                        api.setMessage("OK: L'annonce a été modifiée")
                        api.setData("Aucun")
                        return serializeData(api, format)
                        break
                    case "DELETE":
                        annonceService.delete(id)
                        api.setStatus(200)
                        api.setMessage("OK: L'annonce a été supprimée")
                        api.setData("Aucun")
                        return serializeData(api, format)
                        break
                    default:
                        api.setStatus(405)
                        api.setMessage("Erreur METHOD NOT ALLOWED: La méthode de la requête n'est pas acceptée")
                        api.setData("Aucun")
                        return serializeData(api, format)
                        break
                }
            }
        }
        api.setStatus(406)
        api.setMessage("Erreur NOT ACCEPTABLE: Impossible de servir une réponse")
        api.setData("Aucun")
        return serializeData(api, format)
    }

    def serializeData(object, format)
    {
        switch (format)
        {
            case 'json':
            case 'application/json':
            case 'text/json':
                return object as JSON
                break
            case 'xml':
            case 'application/xml':
            case 'text/xml':
                return object as XML
                break
        }
    }
}
