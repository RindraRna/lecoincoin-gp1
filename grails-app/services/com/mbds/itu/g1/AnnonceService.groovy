package com.mbds.itu.g1

import grails.gorm.transactions.Transactional

/*import grails.gorm.services.Service*/

@Transactional
class AnnonceService{
    Annonce get(Serializable id){
        Annonce.get(id)
    }

    List<Annonce> recherche(titre, description, params){
        Annonce.findAllByTitleIlikeOrDescriptionIlike(titre, description, [sort: "dateCreated", order: "desc", max: 10, offset: params])
    }

    List<Annonce> list(params){
        Annonce.list([sort: "dateCreated", order: "desc", max: 10, offset: params])
    }

    List<Annonce> getAnnonces(){
        Annonce.list()
    }

    Annonce getById(Serializable id){
        Annonce.get(id)
    }

    void delete(Serializable id){
        Annonce.get(id).delete()
    }

    Annonce save(Annonce annonce){
        annonce.save()
    }

    long count(){
        Annonce.count()
    }

    int pourcentage(long x, long somme){
        return x * 100/somme;
    }

    def modifier(Annonce ancienneAnnnonce, Annonce annonceModifie){
        ancienneAnnnonce.title = annonceModifie.title
        ancienneAnnnonce.description = annonceModifie.description
        ancienneAnnnonce.price = annonceModifie.price
        ancienneAnnnonce.save()
    }

    Annonce enleverLesNull(Annonce annonce1, Annonce annonce2){
        if(annonce2.title == null) annonce2.title = annonce1.title
        if(annonce2.description == null) annonce2.description = annonce1.description
        if(annonce2.price == null) annonce2.price = annonce1.price
        return annonce2
    }

}
