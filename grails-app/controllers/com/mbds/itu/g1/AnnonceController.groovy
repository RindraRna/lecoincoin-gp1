package com.mbds.itu.g1

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.springframework.web.servlet.ModelAndView

import static org.springframework.http.HttpStatus.*

class AnnonceController {
    AnnonceService annonceService
    def springSecurityService
    def userService

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def recherche(){
        def userConnecte = springSecurityService.getCurrentUser()
        def motCle = "%"+params.motCle+"%"
        def annoncesTrouvees = annonceService.recherche(motCle, motCle, params.offset)
        def nbAnnoncesTrouvees =  annoncesTrouvees.size()
       // def message = nbAnnoncesTrouvees+" annonce(s) trouvée(s) pour la recherche de '"+params.motCle+"'"
        flash.messageRecherche=nbAnnoncesTrouvees+" annonce(s) trouvée(s) pour la recherche de '"+params.motCle+"'"
        return new ModelAndView("/annonce/recherche", [ nbAnnonce: nbAnnoncesTrouvees, annonces: annoncesTrouvees, pathIllustration: grailsApplication.config.annonces.illustrations.url])

    }

    @Secured(['ROLE_ADMIN'])
    def Supprimer(){
        def userConnecte = springSecurityService.getCurrentUser()
        def idAnnonce = params.idAnnonce
        annonceService.delete(idAnnonce)
        flash.message="L'annonce a été supprimée"
       // return new ModelAndView("/menu/annonce", [nomUserConnecte: userConnecte.username, nbAnnonce: annonceService.count(), annonces: annonceService.list(params.offset), pathIllustration: grailsApplication.config.annonces.illustrations.url, message: "L'annonce a été supprimée"])
        redirect(controller: 'menu', action: 'annonce')
    }

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def Modifier(){
        def userConnecte = springSecurityService.getCurrentUser()
        /*recupérer paramètre*/
        def idAnnonce = params.idAnnonce
        def titre = params.titre
        def prix = params.prix
        def description = params.description
        def idAuteur = params.auteur

        def annonceActuelle = annonceService.get(idAnnonce)
        annonceActuelle.title = titre
        annonceActuelle.price = Double.parseDouble(prix)
        annonceActuelle.description = description
        def userChoisi = userService.get(idAuteur)
        annonceActuelle.author = userChoisi

//        upload fichier
        request.getFiles("illustration[]").each { file ->
            try{
                if(!file.empty){
                    def cheminDestination = grailsApplication.config.annonces.illustrations.path+file.originalFilename
                    file.transferTo(new File(cheminDestination))
                    annonceActuelle.addToIllustrations(new Illustration(filename: file.originalFilename))
                }
            }
            catch(Exception e){
                throw new Exception("Erreur ajout de l'illustration dans l'annonce:"+e.getMessage())
            }
        }

//        update bdd
        try{
            annonceService.save(annonceActuelle)
            flash.message="Votre annonce a été modifiée"
        }
        catch (Exception e){
            throw new Exception("Erreur ajout de l'annonce:"+e.getMessage())
        }
        //return new ModelAndView("/menu/annonce", [nomUserConnecte: userConnecte.username, nbAnnonce: annonceService.count(), annonces: annonceService.list(params.offset), pathIllustration: grailsApplication.config.annonces.illustrations.url, message: "Votre annonce a été modifiée"])
        redirect(controller: 'menu', action: 'annonce')
    }

    @Secured(['ROLE_ADMIN'])
    def Ajouter(){
        /*recupérer paramètre*/
        def titre = params.titre
        def prix = params.prix
        def description = params.description

        def annonceInstance = new Annonce(
                title: titre,
                description: description,
                price: prix
        )
        def userConnecte = springSecurityService.getCurrentUser()
        annonceInstance.author = userConnecte

//        upload fichier
        request.getFiles("illustration[]").each { file ->
            def cheminDestination = grailsApplication.config.annonces.illustrations.path+file.originalFilename
            try{
                file.transferTo(new File(cheminDestination))
                annonceInstance.addToIllustrations(new Illustration(filename: file.originalFilename))
            }
            catch(Exception e){
                throw new Exception("Erreur ajout de l'illustration dans l'annonce:"+e.getMessage())
            }
        }
        /*if (f.empty) {
            flash.message = 'file cannot be empty'
            render(view: 'uploadForm')
            return
        }*/
        //response.sendError(200, 'Done')

//        update bdd
        try{
            annonceService.save(annonceInstance)
            flash.message= "Votre annonce a été ajoutée"
        }
        catch (Exception e){
            throw new Exception("Erreur ajout de l'annonce:"+e.getMessage())
        }
       // return new ModelAndView("/menu/annonce", [nomUserConnecte: userConnecte.username, nbAnnonce: annonceService.count(), annonces: annonceService.list(params.offset), pathIllustration: grailsApplication.config.annonces.illustrations.url, message: "Votre annonce a été ajoutée"])
        redirect(controller: 'menu', action: 'annonce')
    }


    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def versModifier(){
        def userConnecte = springSecurityService.getCurrentUser()
        def idAnnonce = params.idAnnonce
        def annonce = annonceService.get(idAnnonce)
        return new ModelAndView("/annonce/versModifier", [nomUserConnecte: userConnecte.username, users: userService.list(), actionPage: "Modifier", pathIllustration: grailsApplication.config.annonces.illustrations.url, annonce: annonce])
    }

    @Secured(['ROLE_ADMIN'])
    def versAjouter(){
        def userConnecte = springSecurityService.getCurrentUser()
        return new ModelAndView("/annonce/versModifier", [nomUserConnecte: userConnecte.username, actionPage: "Ajouter", pathIllustration: grailsApplication.config.annonces.illustrations.url])
    }
}
