package com.mbds.itu.g1

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured
//import io.micronaut.http.annotation.Delete

import javax.servlet.http.HttpServletResponse
//import io.micronaut.http.client.annotation.Client

@Secured('ROLE_ADMIN')
//@Client("http://localhost:8080/Lecoincoin/api/")
class ApiController {
    def illustrationService
    def apiService
    def userService

    //@Delete("/illustration?id={idIllustration}")
    /*def illustration(){
        switch (request.getMethod()) {
            case "DELETE":
                if (!params.id){
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                }
                illustrationService.delete(params.id)
                return response.status = 200
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }*/

//    GET / PUT / PATCH / DELETE
//    url : localhost:8080/Lecoincoin/api/annonce/{id}
    def annonce() {
        Annonce annonceModifie = new Annonce()
        def titre = request.JSON.title
        def description = request.JSON.description
        def prix = request.JSON.price
        annonceModifie.title = titre
        annonceModifie.description = description
        annonceModifie.price = prix
        render apiService.annonce(request.getMethod(), params.id, request.getHeader("Accept"), annonceModifie)
    }

//    GET / POST
//    url : localhost:8080/Lecoincoin/api/annonces
    def annonces() {
        Annonce nouvelleAnnonce = new Annonce()
        def titre = request.JSON.title
        def description = request.JSON.description
        def prix = request.JSON.price
        def auteurId = null
        if(request.JSON.author != null){
            auteurId = request.JSON.author.id
        }
        if(titre && description && prix && auteurId){
            nouvelleAnnonce.title = titre
            nouvelleAnnonce.description = description
            nouvelleAnnonce.price = prix
            def auteur = userService.get(auteurId)
            nouvelleAnnonce.author = auteur
        }
        render apiService.annonces(request.getMethod(), request.getHeader("Accept"), nouvelleAnnonce)
    }

//    GET / PUT / PATCH / DELETE
//    url : localhost:8080/Lecoincoin/api/user/{id}
    def user() {
        User userInstance = new User()
        def username = request.JSON.username
        def password = request.JSON.password
        def role = request.JSON.role
        userInstance.username = username
        userInstance.password = password
        render apiService.user(request.getMethod(), params.id, request.getHeader("Accept"), userInstance, role)
    }

//    GET / POST
//    url : localhost:8080/Lecoincoin/api/users
    def users() {
        User nouveauUser = new User()
        def username = request.JSON.username
        def password = request.JSON.password
        def role = request.JSON.role
        if(username && username){
            nouveauUser.username = username
            nouveauUser.password = password
        }
        render apiService.users(request.getMethod(), request.getHeader("Accept"), nouveauUser, role)
    }
}
