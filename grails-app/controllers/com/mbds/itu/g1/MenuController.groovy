package com.mbds.itu.g1

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.web.servlet.ModelAndView

class MenuController {
    static defaultAction = "tableauDeBord"
    def annonceService
    def userService
    def illustrationService

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def tableauDeBord() {
        def mytheme=session["theme"]
        if (!mytheme)
        {
            session["theme"] = "linear-gradient(180deg,#4e73df 10%,#224abe 100%)"
        }
         List<Role> listRole = userService.listRole()
        int nbAdministrateur =  userService.findByRole(listRole.get(0));
        int nbModerateur =  userService.findByRole(listRole.get(1));
        //return new ModelAndView("/menu/tableauDeBord")
        long somme = annonceService.count() + userService.count() + illustrationService.count() + userService.countRole();
        return new ModelAndView("/menu/tableauDeBord",
                [nbAnnonce: annonceService.count(), nbUser: userService.count(), nbIllustration: illustrationService.count(), nbRole: userService.countRole(),
                 somme: somme, nbAdministrateur: nbAdministrateur, nbModerateur: nbModerateur,
                annonce: annonceService.pourcentage(annonceService.count(), somme), user: annonceService.pourcentage(userService.count(), somme),
                 illustration: annonceService.pourcentage(illustrationService.count(), somme), role: annonceService.pourcentage(userService.countRole(), somme),
                 message: "aucun"])
    }

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def themeNoir() {
        session["theme"] = "linear-gradient(180deg,#545353 10%,black 100%)"
        List<Role> listRole = userService.listRole()
        int nbAdministrateur =  userService.findByRole(listRole.get(0));
        int nbModerateur =  userService.findByRole(listRole.get(1));
        //return new ModelAndView("/menu/tableauDeBord")
        long somme = annonceService.count() + userService.count() + illustrationService.count() + userService.countRole();
        return new ModelAndView("/menu/tableauDeBord",
                [nbAnnonce: annonceService.count(), nbUser: userService.count(), nbIllustration: illustrationService.count(), nbRole: userService.countRole(),
                 somme: somme, nbAdministrateur: nbAdministrateur, nbModerateur: nbModerateur,
                 annonce: annonceService.pourcentage(annonceService.count(), somme), user: annonceService.pourcentage(userService.count(), somme),
                 illustration: annonceService.pourcentage(illustrationService.count(), somme), role: annonceService.pourcentage(userService.countRole(), somme),
                 message: "aucun"])
    }

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def themeBleu() {
        session["theme"] = "linear-gradient(180deg,#4e73df 10%,#224abe 100%)"
        List<Role> listRole = userService.listRole()
        int nbAdministrateur =  userService.findByRole(listRole.get(0));
        int nbModerateur =  userService.findByRole(listRole.get(1));
        //return new ModelAndView("/menu/tableauDeBord")
        long somme = annonceService.count() + userService.count() + illustrationService.count() + userService.countRole();
        return new ModelAndView("/menu/tableauDeBord",
                [nbAnnonce: annonceService.count(), nbUser: userService.count(), nbIllustration: illustrationService.count(), nbRole: userService.countRole(),
                 somme: somme, nbAdministrateur: nbAdministrateur, nbModerateur: nbModerateur,
                 annonce: annonceService.pourcentage(annonceService.count(), somme), user: annonceService.pourcentage(userService.count(), somme),
                 illustration: annonceService.pourcentage(illustrationService.count(), somme), role: annonceService.pourcentage(userService.countRole(), somme),
                 message: "aucun"])
    }

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def annonce(){
        /*respond annonceService.list(params), model:[pathIllustration: grailsApplication.config.annonces.illustrations.url]*/
        return new ModelAndView("/menu/annonce", [nbAnnonce: annonceService.count(), annonces: annonceService.list(params.offset), pathIllustration: grailsApplication.config.annonces.illustrations.url])
        /*return new ModelAndView("/menu/annonce", [nbAnnonce: annonceService.count(), annonces: annonceService.list(params.offset), pathIllustration: grailsApplication.config.annonces.illustrations.url, message: "aucun"])*/
    }
    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def utilisateur(){
        params.max = GlobalConfig.itemsPerPage()
        return new ModelAndView("/menu/utilisateur", [utilisateurs: UserRole.list(params), userCount:UserRole.count()])
    }

}
