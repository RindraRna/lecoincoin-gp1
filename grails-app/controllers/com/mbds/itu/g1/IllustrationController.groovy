package com.mbds.itu.g1

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.web.servlet.ModelAndView

class IllustrationController {
    def illustrationService
    def annonceService
    def userService
    def springSecurityService

    @Secured(['ROLE_ADMIN'])
    def supprimer(){
        def userConnecte = springSecurityService.getCurrentUser()
        def idIllustration = params.idIllustration
        def idAnnonce = params.idAnnonce
        illustrationService.delete(idIllustration)
        def annonce = annonceService.get(idAnnonce)
        flash.message = "Vous avez supprimé l'illustration"
        return new ModelAndView("/annonce/versModifier", [nomUserConnecte: userConnecte.username, users: userService.list(), actionPage: "Modifier", pathIllustration: grailsApplication.config.annonces.illustrations.url, annonce: annonce])
    }
}
