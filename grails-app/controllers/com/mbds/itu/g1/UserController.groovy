package com.mbds.itu.g1

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import org.springframework.web.servlet.ModelAndView

class UserController {
    def userService
    def springSecurityService
    def passwordEncoder

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def show(Long id) {
        respond userService.get(id)
    }

    @Secured(['ROLE_ADMIN'])
    def Ajouter(){

        /*recupérer paramètre*/
        params.max = GlobalConfig.itemsPerPage()
        def username = params.username
        def password = params.password
        def confirmPassword = params.confirmPassword
        def authority = (params.authority)


        try{
            def authorityValeur=Role.findByAuthority(authority)

            if(password == confirmPassword){
                def instanceUser = new User(username: username, password: password).save()
                UserRole.create(instanceUser,authorityValeur , true)
                flash.message = "L'utilisateur a été ajouté";
            }else{
                flash.message = "Erreur: mot de passe non identiques. L'utilisateur n'a pas été ajouté. ";
            }

        }catch(Exception e){
            throw new Exception("Erreur ajout de l'utilisateur:"+e.getMessage())
        }
        redirect(controller: 'menu', action: 'utilisateur')
    }


    @Secured(['ROLE_ADMIN'])
    def versAjouter(){
        return new ModelAndView("/user/versModifier", [actionPage: "Ajouter" , roles:Role.list()])
    }

    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def versModifier(){
        def idUser=Integer.parseInt(params.idUser)
        def idRole=Integer.parseInt(params.idRole)

        def userConnecte = springSecurityService.currentUser
        def role=Role.get(idRole)
        def userRole=UserRole.get(idUser,idRole)
        def roleVal = userRole.role.authority
        return new ModelAndView("/user/versModifier", [actionPage: "Modifier", userRole: userRole, roles:Role.list(), rolesVal: roleVal, userConnecte: userConnecte ])
    }
    @Secured(['ROLE_ADMIN','ROLE_MODERATEUR'])
    def Modifier(){
        params.max = GlobalConfig.itemsPerPage()
        /*recupérer paramètre*/
        def idUser=Integer.parseInt(params.idUser)
        def idRole=Integer.parseInt(params.idRole)
        def username = params.username
        def password = params.password
        def authority = params.authority
       // def passwordHache=params.passwordHache
        System.out.println(username+" et "+ password+" et "+authority)
        try{

            def roleAuthority=Role.findByAuthority(authority)

            System.out.println(idUser+" id utilisateur et"+roleAuthority.id)
            def user=userService.get(idUser)
            user.username=username
            user.password=password

            userService.save(user)

            UserRole.removeAll(user)
            def role=Role.get(roleAuthority.id)

            System.out.println(user.id+" id utilisateur mbola velona  et role id "+role.id)
            User nouveau=userService.get(user.id)
            UserRole.create(nouveau,role,true)

            flash.message = "L'utilisateur a été modifié";

            def userConnecte = springSecurityService.currentUser
            if(userConnecte == user){
                return redirect(controller: 'logout', action: 'index')
            }
            else{
                return redirect(controller: 'menu', action: 'utilisateur')
            }
        }catch(Exception e){
            throw new Exception("Erreur modification de l'utilisateur:"+e.getMessage())
        }
    }


    @Secured('ROLE_ADMIN')
    def modiferMotDePasse(){
        def idUser = params.idUser
        def idRole = params.idRole
        def motDePasseActuel = params.motDePasseActuel
        User userConnecte = springSecurityService.getCurrentUser()
        if(springSecurityService.passwordEncoder.isPasswordValid(userConnecte.getPassword(), motDePasseActuel, null)){
            def nouveauMotDePasse = params.nouveauMotDePasse
            def motDePasseConfirme = params.motDePasseConfirme
            if(nouveauMotDePasse == motDePasseConfirme){
                User userModifie = new User()
                userModifie.password = nouveauMotDePasse
                User userSansNull = userService.enleverLesNull(userConnecte, userModifie)
                /*modifier user*/
                userService.modifier(userConnecte, userSansNull)

                /*modifiere role user*/
                def role = params.role
                def roleAuthority = Role.findByAuthority(role)
                UserRole.removeAll(userConnecte)
                def role2 = Role.get(roleAuthority.id)
                User nouveauUser = userService.get(userConnecte.id)
                UserRole.create(nouveauUser, role2,true)

                flash.message = "Le mot de passe a été modifié"
                redirect(controller: 'logout', action: 'index')
                return
            }
            else{
                flash.message = "Erreur: Le mot de passe de confirmation n'est pas identique au mot de passe saisi"
                return redirect(controller: 'user', action: 'versModifier', params: [idUser: idUser, idRole: idRole])
            }
        }
        else{
            flash.message = "Erreur: Le mot de passe actuel est incorrect"
            return redirect(controller: 'user', action: 'versModifier', params: [idUser: idUser, idRole: idRole])
        }
    }


    @Secured('ROLE_ADMIN')
    def Supprimer() {
        params.max = GlobalConfig.itemsPerPage()
        def idUser=Integer.parseInt(params.idUser)
        def idRole=Integer.parseInt(params.idRole)
        if (idUser == null || idRole==null) {
            notFound()
            return
        }
        try{

            def user=userService.get(idUser)
            def role = Role.get(idRole)
            UserRole.remove(user,role)
            flash.message = "L'utilisateur a été supprimé";

        }catch(Exception e){
            throw new Exception("Erreur suppression de l'utilisateur:"+e.getMessage())
        }
        redirect(controller: 'menu', action: 'utilisateur')

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
