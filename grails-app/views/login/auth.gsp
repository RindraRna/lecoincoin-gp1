<%--
  Created by IntelliJ IDEA.
  User: Mihariniaina
  Date: 08/03/2021
  Time: 23:34
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Login</title>
    <meta name="layout" content="${gspLayout ?: 'main'}"/>
    <style>
        .message{
            text-align: center;
            color: #4c71dd;
            font-weight: bold;
            background-color: revert;
            border-color: blue;
        }

        .card-title{
            height: 4.375rem;
            font-family: Nunito,-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
            text-decoration: none;
            font-size: 1rem;
            font-weight: 800;
            padding: 1.5rem 1rem;
            text-align: center;
            text-transform: uppercase;
            letter-spacing: .05rem;
            z-index: 1;

        }
        .card-title svg{
            font-size: 1rem;
            font-weight: 900;
        }
    </style>
</head>
    <body>
        <div class="row" style="top: 30%;">
            <div class="col-md-3"></div>
            <div class="col-md-6" style="background-image: linear-gradient(180deg,rgba(248,248,248,0.5) 10%,rgba(180,217,246, 0.5) 100%); border-radius: 10px; ">
                <div class="card card-signin my-5" style=" margin: 10px;">
                    <div class="card-body">
                        </br>
                        <h5 class="card-title text-center"><i class="fas fa-laugh-wink"></i> Se connecter à lecoicoin</h5>
                        </br>
                        %{--<g:if test="${flash.message}">
                            <div class="alert alert-success alert-dismissible message">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>${flash.message}</strong>
                            </div>
                        </g:if>--}%
                        <g:if test='${flash.message}'>
                            <div class="alert alert-danger" role="alert">${flash.message}</div>
                        </g:if>
                        <form class="form-signin" action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" autocomplete="off">
                            <div class="form-group">
                                <label for="username">Nom</label>
                                <input type="text" class="form-control" name="${usernameParameter ?: 'username'}" id="username" autocapitalize="none" value="Gérard"/>
                            </div>

                            <div class="form-group">
                                <label for="password">Mot de passe</label>
                                <input type="password" class="form-control" name="${passwordParameter ?: 'password'}" id="password" value="password"/>
                                <i id="passwordToggler" title="toggle password display" onclick="passwordDisplayToggle()">&#128065;</i>
                            </div>

                            <div class="form-group form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="${rememberMeParameter ?: 'remember-me'}" id="remember_me" <g:if test='${hasCookie}'>checked="checked"</g:if>/> Se souvenir de moi
                                </label>
                            </div>
                            <button id="submit"  class="btn btn-lg btn-default btn-block text-uppercase" type="submit">Se connecter</button>
                            <hr class="my-4">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>


        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function(event) {
                document.forms['loginForm'].elements['username'].focus();
            });
            function passwordDisplayToggle() {
                var toggleEl = document.getElementById("passwordToggler");
                var eyeIcon = '\u{1F441}';
                var xIcon = '\u{2715}';
                var passEl = document.getElementById("password");
                if (passEl.type === "password") {
                    toggleEl.innerHTML = xIcon;
                    passEl.type = "text";
                } else {
                    toggleEl.innerHTML = eyeIcon;
                    passEl.type = "password";
                }
            }
        </script>
    </body>
</html>