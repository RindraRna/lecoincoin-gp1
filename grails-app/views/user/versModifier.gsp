<%--
  Created by IntelliJ IDEA.
  User: rna
  Date: 2021-03-05
  Time: 20:57
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="baseLayout" />
    <title>Lecoincoin_Modifier un utilisateur</title>
    <style type="text/css">
        .message{
            text-align: center;
            color: #4c71dd;
            font-weight: bold;
            background-color: revert;
            border-color: blue;
        }

        .btnMilieu{
            margin-top: 15px;
            text-align: center;
        }

        .margeGauche{
            margin-left: 108px;
        }

        .btn-full{
           width: 100%;
        }

        .cacher{
            display: none;
        }
    </style>
</head>

<body>
<!-- Begin Page Content -->
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success alert-dismissible message">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>${flash.message}</strong>
        </div>
    </g:if>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">${actionPage} un utilisateur</h1>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Card Example -->
            <div class="card shadow mb-12">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <g:form controller="user" action="${actionPage}" method="POST">
                                <div>
                                    <g:if test="${actionPage == 'Modifier'}">

                                        <g:hiddenField name="idUser" value="${userRole.user.id}"/>
                                        <g:hiddenField name="idRole" value="${userRole.role.id}"/>

                                    </g:if>
                                    <div class="col-md-12">
                                        <div>
                                            <label class="text-xs font-weight-bold text-primary text-uppercase">Nom utilisateur</label><br/>
                                            <g:if test="${actionPage == 'Ajouter'}">

                                                <input class="form-control" type="text" name="username" required="true"/>
                                            </g:if>
                                            <g:else>
                                                <input class="form-control" type="text" name="username" required="true" value="${userRole.user.username}"/>
                                            </g:else>
                                        </div>
                                        <div>

                                            <g:if test="${actionPage == 'Ajouter'}">

                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Mot de passe</label><br/>
                                                <input class="form-control" type="password" id="txtPassword" name="password" required="true" onkeyup="Validate()"/>

                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Confirmer le mot de passe</label><br/>
                                                <input class="form-control" type="password" id="txtConfirmPassword" required="true" onkeyup="Validate()" name="confirmPassword"/>
                                                <div id="message" role="status"></div>
                                            </g:if>
                                            <g:else>

                                                <g:hiddenField name="password" value="${userRole.user.password}"/>
                                            </g:else>
                                        </div>

                                        <div class="form-group">

                                            <label class="text-xs font-weight-bold text-primary text-uppercase">Role</label><br/>

                                            <select name="authority" class="form-control"  required>
                                                <g:if test="${ actionPage == 'Modifier' }">
                                                    <option value="${rolesVal}">${rolesVal}</option>
                                                </g:if>
                                                <g:each var="${role}" in="${roles}">
                                                    <g:if test="${rolesVal != role.authority}">
                                                        <option value="${role.authority}">${role.authority}</option>
                                                    </g:if>
                                                </g:each>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="btnMilieu">
                                    <g:link controller="menu" action="utilisateur" class="btn btn-secondary btn-icon-split">
                                        <span class="icon text-gray-600">
                                            <i class="fas fa-arrow-left"></i>
                                        </span>
                                        <span class="text">Annuler</span>
                                    </g:link>
                                    <button class="btn btn-primary btn-icon-split">
                                        <span class="icon text-gray-600">
                                            <i class="fas fa-check"></i>
                                        </span>
                                        <span class="text">Valider</span>
                                    </button>
                                </div>
                            </g:form>
                            <g:if test="${ actionPage == 'Modifier' && userRole.user == userConnecte}">
                                <br/>
                                <p>Si vous voulez changer le mot de passe de ${userRole.user.username}, cliquer sur</p>
                                <div class="margeGauche">
                                    <button class="btn btn-primary btn-icon-split" onclick="montrerFormMotDePasse()">
                                        <span class="icon text-gray-600">
                                            <i class="fas fa-lock"></i>
                                        </span>
                                        <span class="text">Changer le mot de passe</span>
                                    </button>
                                </div>
                                <div class="cacher" id="changerMdp">
                                    <br/>
                                    <h4>Changer le mot de passe de ${ userConnecte.username }</h4>
                                    <g:form controller="user" action="modiferMotDePasse" method="POST">
                                        <g:hiddenField name="role" value="${rolesVal}"/>
                                        <g:hiddenField name="idUser" value="${userRole.user.id}"/>
                                        <g:hiddenField name="idRole" value="${userRole.role.id}"/>
                                        <div>
                                            <label class="text-xs font-weight-bold text-primary text-uppercase">Mot de passe actuel</label><br/>
                                            <input class="form-control" type="password" name="motDePasseActuel" required="true"/>
                                        </div>
                                        <div>
                                            <label class="text-xs font-weight-bold text-primary text-uppercase">Nouveau mot de passe</label><br/>
                                            <input class="form-control" type="password" id="nouveauMotDePasse" name="nouveauMotDePasse" required="true" onkeyup="Validate2()"/>
                                        </div>
                                        <div>
                                            <label class="text-xs font-weight-bold text-primary text-uppercase">Confirmer le mot de passe</label><br/>
                                            <input class="form-control" type="password" id="motDePasseConfirme" name="motDePasseConfirme" required="true" onkeyup="Validate2()"/>
                                            <div id="message2" role="status"></div>
                                        </div>
                                        <br/>
                                        <button class="btn btn-primary btn-icon-split btn-full">
                                            <span class="icon text-gray-600">
                                                <i class="fas fa-check"></i>
                                            </span>
                                            <span class="text">Valider</span>
                                        </button>
                                    </g:form>
                                </div>
                            </g:if>
                        </div>
                        <div class="offset-md-1 col-md-6">
                            <span class="text-xs font-weight-bold text-primary text-uppercase">Les rôles</span>
                            <p>
                                <strong>ROLE_ADMIN: </strong> Il peut tout faire: créer, modifier, supprimer, lire des annonces, des illustrations et des utilisateurs.
                            </p>
                            <p>
                                <strong>ROLE_MODERATEUR: </strong> Il ne peut que lire et modifier des annonces et des utilisateurs.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">
    function montrerFormMotDePasse(){
        document.getElementById("changerMdp").style.display = "block";
    }

    function Validate() {
        var password = document.getElementById("txtPassword").value;
        var confirmPassword = document.getElementById("txtConfirmPassword").value;
        if (password != confirmPassword) {
            document.getElementById("message").innerHTML = "Le mot de passe et le mot de passe à confirmer ne se correspondent pas.";
            document.getElementById("message").style.color = "red";
        }
        else{
            document.getElementById("message").innerHTML = "Les mots de passe sont identiques.";
            document.getElementById("message").style.color = "green";
        }
    }

    function Validate2() {
        var password = document.getElementById("nouveauMotDePasse").value;
        var confirmPassword = document.getElementById("motDePasseConfirme").value;
        if (password != confirmPassword) {
            document.getElementById("message2").innerHTML = "Le mot de passe et le mot de passe à confirmer ne se correspondent pas.";
            document.getElementById("message2").style.color = "red";
        }
        else{
            document.getElementById("message2").innerHTML = "Les mots de passe sont identiques.";
            document.getElementById("message2").style.color = "green";
        }
    }
</script>
</div>
</body>
</html>