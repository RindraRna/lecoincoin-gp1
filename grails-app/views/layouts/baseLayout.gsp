<%--
  Created by IntelliJ IDEA.
  User: rna
  Date: 2021-03-05
  Time: 08:59
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><g:layoutTitle default="Lecoincoin"/></title>

    <!-- Custom fonts for this template-->
    %{--<link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">--}%

    <asset:stylesheet src="css/sb-admin-2.min.css"/>
    <asset:stylesheet src="vendor/fontawesome-free/css/all.min.css"/>
    <g:layoutHead/>
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar" style="background-image: ${session.getAttribute("theme")}">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/Lecoincoin/">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Lecoincoin</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/Lecoincoin/">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Tableau de bord</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Gestions
        </div>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <g:link controller="Menu" action="annonce" class="nav-link">
                <i class="fas fa-fw fa-list"></i>
                <span>Annonces</span></a>
            </g:link>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <g:link controller="Menu" action="utilisateur" class="nav-link">
                <i class="fas fa-fw fa-user-circle"></i>
                <span>Utilisateurs</span>
            </g:link>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Préférence
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
               aria-expanded="true" aria-controls="collapsePages">
                <i class="fas fa-fw fa-wrench"></i>
                <span>Thème</span>
            </a>
            <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Choix du thème:</h6>
                    <g:link controller="Menu" action="themeNoir" class="collapse-item">
                        Noir
                    </g:link>
                    <g:link controller="Menu" action="themeBleu" class="collapse-item">
                        Bleu
                    </g:link>
                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

    <li class="nav-item">
        <g:link controller="logout" action="index" class="nav-link">
            <i class="fas fa-fw fa-power-off"></i>
            <span>Déconnexion</span>
        </g:link>
    </li>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <g:form controller="annonce" action="recherche" method="GET"
                        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                        <input type="text" name="motCle" class="form-control bg-light border-0 small" placeholder="Recherche annonce selon titre / desription"
                               aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" style="background-image: ${session.getAttribute("theme")}">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </g:form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <sec:ifLoggedIn>
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">  <sec:loggedInUserInfo field='username'/></span>
                            <img class="img-profile rounded-circle"
                                 src="${ grailsApplication.config.annonces.illustrations.url }/avatar.png">
                        </a>
                        </sec:ifLoggedIn>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <g:layoutBody/>

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Group 1 - Grails</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
    <asset:javascript src="vendor/jquery/jquery.min.js"/>
    <asset:javascript src="vendor/bootstrap/js/bootstrap.bundle.min.js"/>

<!-- Core plugin JavaScript-->
    <asset:javascript src="vendor/jquery-easing/jquery.easing.min.js"/>

<!-- Custom scripts for all pages-->
    <asset:javascript src="js/sb-admin-2.min.js"/>

<!-- Page level plugins -->
    <asset:javascript src="vendor/chart.js/Chart.min.js"/>

<!-- Page level custom scripts -->
    <asset:javascript src="js/demo/chart-area-demo.js"/>
    <script type="text/javascript">
        // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        // Pie Chart Example
        var ctx = document.getElementById("myPieChart");
        var myPieChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ["Administrateur", "Modérateur"],
                datasets: [{
                    data: [${nbAdministrateur}, ${nbModerateur}],
                    backgroundColor: ['#4e73df', '#1cc88a'],
                    hoverBackgroundColor: ['#2e59d9', '#17a673'],
                    hoverBorderColor: "rgba(234, 236, 244, 1)",
                }],
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    caretPadding: 10,
                },
                legend: {
                    display: false
                },
                cutoutPercentage: 80,
            },
        });

    </script>

</body>

</html>