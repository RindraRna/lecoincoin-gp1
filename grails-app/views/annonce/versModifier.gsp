<%--
  Created by IntelliJ IDEA.
  User: rna
  Date: 2021-03-05
  Time: 20:57
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="baseLayout" />
    <title>Lecoincoin_Modifier une annonce</title>
    <style type="text/css">
        .btnMilieu{
            margin-top: 15px;
            text-align: center;
        }

        .zoom {
            -webkit-transition: all 0.35s ease-in-out;
            -moz-transition: all 0.35s ease-in-out;
            transition: all 0.35s ease-in-out;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in;
        }

        .zoom:hover,
        .zoom:active,
        .zoom:focus {
            /**adjust scale to desired size,
            add browser prefixes**/
            -ms-transform: scale(2.5);
            -moz-transform: scale(2.5);
            -webkit-transform: scale(2.5);
            -o-transform: scale(2.5);
            transform: scale(2.5);
            position:relative;
            z-index:100;
        }

        .message{
            text-align: center;
            color: #4c71dd;
            font-weight: bold;
            background-color: revert;
            border-color: blue;
        }
    </style>
</head>

<body>
    <!-- Begin Page Content -->
    <div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success alert-dismissible message">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>${flash.message}</strong>
        </div>
    </g:if>
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">${actionPage} une annonce</h1>
        </div>
            <div class="row">
                    <div class="col-md-12">
                        <!-- Basic Card Example -->
                        <div class="card shadow mb-12">
                            <div class="card-body">
                                <g:uploadForm controller="annonce" action="${actionPage}" method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div>
                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Titre</label><br/>
                                                <g:if test="${actionPage == 'Ajouter'}">
                                                    <input class="form-control" type="text" name="titre" required="true"/>
                                                </g:if>
                                                <g:else>
                                                    <input type="hidden" name="idAnnonce" value="${annonce.id}"/>
                                                    <input class="form-control" type="text" name="titre" required="true" value="${annonce.title}"/>
                                                </g:else>
                                            </div>
                                            <div>
                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Prix ($)</label><br/>
                                                <g:if test="${actionPage == 'Ajouter'}">
                                                    <input class="form-control" type="number" min="0" name="prix" step=".01" required="true"/>
                                                </g:if>
                                                <g:else>
                                                    <input class="form-control" type="number" min="0" name="prix" step=".01" required="true" value="${annonce.price}"/>
                                                </g:else>
                                            </div>
                                            <div>
                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Description</label><br/>
                                                <g:if test="${actionPage == 'Ajouter'}">
                                                    <textarea class="form-control" name="description" rows="4" required="true"></textarea>
                                                </g:if>
                                                <g:else>
                                                    <textarea class="form-control" name="description" rows="4" required="true">${annonce.description}</textarea>
                                                </g:else>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <g:if test="${actionPage == 'Modifier'}">
                                                <label class="text-xs font-weight-bold text-primary text-uppercase">Auteur</label><br/>
                                                <select name="auteur" class="form-control">
                                                    <g:each var="${user}" in="${users}">
                                                        <g:if test="${user.id == annonce.author.id}">
                                                            <option value="${user.id}" selected="selected">${user.username}</option>
                                                        </g:if>
                                                        <g:else>
                                                            <option value="${user.id}">${user.username}</option>
                                                        </g:else>
                                                    </g:each>
                                                </select>
                                                <br/>
                                            </g:if>
                                            <label class="text-xs font-weight-bold text-primary text-uppercase">Illustrations</label><br/>
                                            <sec:ifAnyGranted roles='ROLE_ADMIN'>
                                                <input class="form-control" type="file" name="illustration[]" accept="image/*" multiple <g:if test="${actionPage == 'Ajouter'}">required</g:if>/><br/><br/>
                                            </sec:ifAnyGranted>
                                            <g:if test="${actionPage == 'Modifier'}">
                                                <g:each var="${illustration}" in="${annonce.illustrations}">
                                                    <figure class="figure">
                                                        <img class="thumbnail zoom" src = "${pathIllustration}${illustration.filename}" width="175"/>
                                                        <br/>
                                                        <sec:ifAnyGranted roles='ROLE_ADMIN'>
                                                            <figcaption class="figure-caption text-center" onclick="return confirm('Voulez-vous vraiment supprimer pour cette image?')"><g:link class="btn btn-light btn-icon-split" controller="illustration" action="supprimer" params="${[idIllustration: illustration.id, idAnnonce: annonce.id]}">
                                                                    <span class="icon text-white-600">
                                                                        <i class="fas fa-trash"></i>
                                                                    </span>
                                                                    <span class="text">Supprimer</span>
                                                                </g:link>
                                                            </figcaption>
                                                        </sec:ifAnyGranted>
                                                    </figure>
                                                </g:each>
                                            </g:if>
                                        </div>
                                    </div>
                                    <div class="btnMilieu">
                                        <g:link controller="menu" action="annonce" class="btn btn-secondary btn-icon-split">
                                            <span class="icon text-gray-600">
                                                <i class="fas fa-arrow-left"></i>
                                            </span>
                                            <span class="text">Annuler</span>
                                        </g:link>
                                        <button class="btn btn-primary btn-icon-split">
                                            <span class="icon text-gray-600">
                                                <i class="fas fa-check"></i>
                                            </span>
                                            <span class="text">Valider</span>
                                        </button>
                                    </div>
                                </g:uploadForm>
                                %{--</g:form>--}%
                            </div>
                        </div>
                    </div>

            </div>

        </div>
        <!-- /.container-fluid -->
    </div>
</body>
</html>