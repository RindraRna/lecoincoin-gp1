    <!DOCTYPE html>
<html lang="en">

<head>

    <meta name="layout" content="baseLayout" />
    <title>Lecoincoin_Liste utilisateur</title>
    <style type="text/css">
        .message{
            text-align: center;
            color: #4c71dd;
            font-weight: bold;
            background-color: revert;
            border-color: blue;
        }

            .step {
            padding: 10px;
            color: black;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
            }

        .nextLink {
            padding: 10px;
            color: black;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
            }

        .prevLink {
            padding: 10px;
            color: black;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
            }

        .currentStep {
            padding: 10px;
            background-color: #4c71dd;
            color: white;
            border: 1px solid #4c71dd;
        }

        .step.gap {
            display: none;
        }

        .step:hover:not(.active) {
            background-color: #ddd;
        }

        .pagination{
            margin-top: 3%;
            margin-left: 43%;
        }



    </style>

</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrapper">



        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <div class="container-fluid">
                         <g:if test="${flash.message}">
                             <div class="alert alert-success alert-dismissible message">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>${flash.message}</strong>
                               </div>
                          </g:if>


                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Liste des utilisateurs</h1>
                            <sec:ifAnyGranted roles='ROLE_ADMIN'>
                                 <div class="aGauche">
                                     <g:link controller="user" action="versAjouter" class="btn btn-primary btn-icon-split">
                                     <span class="icon text-gray-600">
                                        <i class="fas fa-plus"></i>
                                    </span>
                                 <span class="text">Ajouter un utilisateur</span>
                                    </g:link>
                                 </div>
                            </sec:ifAnyGranted>
                    </div>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <!--<div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                        </div>-->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered"  width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Rôle</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <g:each var="${utilisateur}" in="${utilisateurs}">
                                        <tr>
                                            <td>${utilisateur.user.username}</td>
                                            <td>${utilisateur.role.authority}</td>
                                            <td>
                                                    <g:link  title="modifier utilisateur" action="versModifier" controller="user" params="${[idUser: utilisateur.user.id,idRole:utilisateur.role.id]}"> <i class="fas fa-fw fa-edit"></i></g:link>

                                                <sec:ifAnyGranted roles='ROLE_ADMIN'>
                                                    <g:link onclick="return confirm('Etes-vous sur de vouloir supprimer ?')" title="supprimer utilisateur" action="Supprimer" controller="user" params="${[idUser: utilisateur.user.id,idRole:utilisateur.role.id]}"><i class="fas fa-fw fa-trash"></i></g:link>

                                                </sec:ifAnyGranted>
                                            </td>

                                        </tr>
                                    </g:each>


                                    </tbody>
                                </table>
                                <div class="pagination ">

                                <g:paginate controller="menu" action="utilisateur" total="${userCount}" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->



        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->






</body>

</html>