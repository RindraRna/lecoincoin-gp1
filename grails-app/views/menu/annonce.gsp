<%--
  Created by IntelliJ IDEA.
  User: rna
  Date: 2021-03-05
  Time: 10:18
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="baseLayout" />
    <title>Lecoincoin_Annonce</title>
    <style type="text/css">
        .aGauche{
            float: left;
        }

        .zoom {
            -webkit-transition: all 0.35s ease-in-out;
            -moz-transition: all 0.35s ease-in-out;
            transition: all 0.35s ease-in-out;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in;
        }

        .zoom:hover,
        .zoom:active,
        .zoom:focus {
            /**adjust scale to desired size,
            add browser prefixes**/
            -ms-transform: scale(2.5);
            -moz-transform: scale(2.5);
            -webkit-transform: scale(2.5);
            -o-transform: scale(2.5);
            transform: scale(2.5);
            position:relative;
            z-index:100;
        }

        .message{
            text-align: center;
            color: #4c71dd;
            font-weight: bold;
            background-color: revert;
            border-color: blue;
        }

        .step {
            padding: 10px;
            color: black;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
        }

        .nextLink {
            padding: 10px;
            color: black;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
        }

        .prevLink {
            padding: 10px;
            color: black;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
        }

        .currentStep {
            padding: 10px;
            background-color: #4c71dd;
            color: white;
            border: 1px solid #4c71dd;
        }

        .step.gap {
            display: none;
        }

        .step:hover:not(.active) {
            background-color: #ddd;
        }

        .pagination{
            margin-top: 3%;
            margin-left: 43%;
        }
    </style>
</head>

<body>
    <!-- Begin Page Content -->
    <div class="container-fluid">
         <g:if test="${flash.message}">
             <div class="alert alert-success alert-dismissible message">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                 <strong>${flash.message}</strong>
             </div>
         </g:if>
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Liste des annonces</h1>
            <sec:ifAnyGranted roles='ROLE_ADMIN'>
                <div class="aGauche">
                    <g:link controller="annonce" action="versAjouter" class="btn btn-primary btn-icon-split">
                        <span class="icon text-gray-600">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Ajouter une annonce</span>
                    </g:link>
                </div>
            </sec:ifAnyGranted>
        </div>

        <div class="row">
            <g:each var="${annonce}" in="${annonces}">
                <div class="col-md-12">
                    <!-- Basic Card Example -->
                    <div class="card shadow mb-12">
                        <div class="card-header py-12">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <g:link controller="annonce" action="versModifier" params="${[idAnnonce: annonce.id]}">
                                    ${annonce.title}
                                </g:link>
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    ${annonce.description}
                                    <br/><br/>
                                    <ul>
                                        <li><strong>Prix:</strong> <g:formatNumber number="${annonce.price}" type="currency" currencyCode="EUR" /></li>
                                        <li><strong>Date de l'annonce:</strong> <g:formatDate format="dd MMM yyyy" date="${annonce.dateCreated}"/></li>
                                    </ul>
                                    <g:link controller="annonce" action="versModifier" params="${[idAnnonce: annonce.id]}" class="btn btn-primary btn-icon-split">
                                        <span class="icon text-gray-600">
                                            <i class="fas fa-pencil-alt"></i>
                                        </span>
                                        <span class="text">Modifier</span>
                                    </g:link>
                                    <sec:ifAnyGranted roles='ROLE_ADMIN'>
                                        <g:link controller="annonce" action="Supprimer" params="${[idAnnonce: annonce.id]}">
                                            <button onclick="return confirm('Voulez-vous vraiment supprimer pour cette annonce?')" class="btn btn-secondary btn-icon-split">
                                                <span class="icon text-gray-600">
                                                    <i class="fas fa-trash"></i>
                                                </span>
                                                <span class="text">Supprimer</span>
                                            </button>
                                        </g:link>
                                    </sec:ifAnyGranted>
                                </div>
                                <div class="col-md-4">
                                    <img class="thumbnail zoom" src = "${pathIllustration}${annonce.illustrations.filename[0]}" alt="Aucune image correspondante" width="175"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </g:each>

        </div>

    </div>
    <!-- /.container-fluid -->
    <div class="pagination ">
        <g:paginate next="Suivant" prev="Précédent" total="${nbAnnonce}" max="10" controller="menu" action="annonce"/>
    </div>

</body>
</html>